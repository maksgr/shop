function postProducts(is_delete=false, resp) {
    var form = $('#form-tovar');
    var t = $('#response-text').val();
    var submit = $('#submit_btn_response');
    var product_id = submit.data("product");
    var name = submit.data('name');
    console.log(t);

    var data = {};

    data.product_id = product_id;
    data.t = t;

    var csrf_token = $('#form_button_product [name=csrfmiddlewaretoken]').val();
    data['csrfmiddlewaretoken'] = csrf_token;

    if(is_delete){
        data['is_delete'] = true;
        resp = $(this).data('resp');
        console.log(resp)
    }

    var url = form.attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        cache: true,
        success: function (data) {
            console.log("OK");
            $('.ajax-coment').append('<li style="list-style-type: none">' +
                '<samp style="font-size: 25px;">' + t + '</samp>' +
                '<button class="btn btn-info btn-icon-anim btn-circle" style="float: right;"><i  id="deleteRespones" class="icon-trash" data-product_id="' +id+ '"></i></button>' +
                '<div style="clear: both;">&nbsp;</div>' +
                '</li>');

        },
        error: function () {
            console.log("Error")
        }
    });
}

$(document).on('click', '#deleteRespones', function (e) {
    e.preventDefault();
    deleteRes = $(this).data('product_id');
    postProducts(is_delete = true, deleteRes);
    $(this).closest('li').remove();
});