function application(is_delete=false, deleteRes) {
    var form = $('#form_button_product');
    var number = $('#number').val();
    var submit_btn = $('#submit_btn');
    var product_id = submit_btn.data("product");
    var product_name = submit_btn.data("name");
    var prodict_price = submit_btn.data("price");

    var data = {};

    data.product_id = product_id;
    data.number = number;

    var csrf_token = $('#form_button_product [name=csrfmiddlewaretoken]').val();
    data['csrfmiddlewaretoken'] = csrf_token;

    if(deleteRes || is_delete){
        data['is_delete'] = true;
        data['deleteRes'] = deleteRes;
    }

    var url = form.attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        cache: true,
        success: function (data) {
            console.log("OK");
            console.log(data.product_total_nmb);
            console.log(data.productionspr);
            if (data.product_total_nmb) {
                $('.top-nav-icon-badge').text(data.product_total_nmb);
                console.log(data.productionspr);
                $('.sl-item').html("");
                $.each(data.productionspr, function (k, v) {
                    $('.sl-item').append('<li>' +
                        '<br/>' + '<samp class="">Назва:  </samp>' + v.name_product +
                        '<br/>' + '<samp class="">Кількість:  </samp>' + v.ammount +
                        '<a data-count="1" data-id="1" class="inline-block font-11 pull-right notifications-time delete-icon" href="#" data-product_id="' +v.id+ '">X</a>' +
                        '<br/>' + '<samp class="">Ціна:  </samp>' + v.price + '$' +
                        '</li>');
                });
            }
        },
        error: function () {
            console.log("Error")
        }
    });

}
$(document).on('click', '.delete-icon', function (e) {
    e.preventDefault();
    deleteRes = $(this).data('product_id');
    application(is_delete = true, deleteRes);
    $(this).closest('li').remove();
});
