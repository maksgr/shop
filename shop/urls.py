"""UniversitySiteLNU URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('select2/', include('django_select2.urls')),
    #path('accounts/login/', include('django.contrib.auth.views')),
    path('catalog/', include('catalog.urls')),
    path('avatar/', include('avatar.urls')),
    #path('media/', include('blobstore_storage.urls')),
    path('', TemplateView.as_view(template_name='registration/profile.html'), name='profile'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
