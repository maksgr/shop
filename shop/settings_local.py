# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'lnu',  # Database name
        'USER': 'root',  # User name
        'PASSWORD': 'root',  # User password
        'HOST': '127.0.0.1',
        'PORT': 3306,
        'OPTIONS':{
            'charset':'utf8mb4',
        },
        #'CHARSET': 'utf8mb4',
        #'COLLATION': 'utf8mb4_unicode_ci',
    }
}