from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib import admin
from System.models import User

# Register your models here.

class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User

class MyUserAdmin(UserAdmin):
    form = MyUserChangeForm

    fieldsets = (
        # (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff',
                                       'groups')}),
    )


admin.site.register(User, MyUserAdmin)

# admin.site.register(User, UserAdmin)
# @admin.register(User, UserAdmin)
# class UserAdmin(admin.ModelAdmin):
#     exclude = ('last_login', 'user_permissions')