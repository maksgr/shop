from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager, Group
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core import validators
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from catalog.models import Money

class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom user realization based on Django AbstractUser and PermissionMixin.
    """
    document = models.FileField(upload_to='documents/%Y/%m/%d/', blank=True, null=True)
    # password =
    email = models.EmailField(_('email address'), unique=True,
                              error_messages={
                                  'unique': "Користувач з такою електронною поштою вже існує",
                              }, null=True, blank=True)

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=30)
    middle_name = models.CharField(max_length=64, verbose_name='по батькові', null=True)

    phone = models.CharField(max_length=64, verbose_name='Особистий телефон',null=True, blank=True, )
    birthday = models.DateField(null=True, blank=True, verbose_name="День народження")

    # system field
    username = models.CharField(_('username'), max_length=30, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, digits and '
                                            '@/./+/-/_ only.'),
                                validators=[
                                    validators.RegexValidator(r'^[\w.@+-]+$',
                                                              _('Enter a valid username. '
                                                                'This value may contain only letters, numbers '
                                                                'and @/./+/-/_ characters.'), 'invalid'),
                                ],
                                error_messages={
                                    'unique': _("A user with that username already exists."),
                                })
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))

    objects = UserManager()

    # this stuff is needed to use this model with django auth as a custom user class
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    # def is_allowed_group(self, groupname='Адміністратори'):
    #         return self.groups.filter(name=groupname).exists()

    def is_admin(self):
        return self.groups.filter(name='Адміністратори').exists()

    def is_owner(self):
        return self.groups.filter(name='Власники').exists()

    def get_money(self):
        return Money.objects.get(owner=self).cash

    # def is_owner(self):
    #     return self.is_owner_group('Власники')

    # def is_director_group(self, groupname='Директори'):
    #     return self.groups.filter(name=groupname).exists()

    def is_director(self):
        return self.groups.filter(name='Директори').exists()

    # def is_accountant_group(self, groupname='Бухгалтери'):
    #     return self.groups.filter(name=groupname).exists()

    def is_accountant(self):
        return self.groups.filter(name='Бухгалтери').exists()