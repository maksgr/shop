from django import forms
from catalog.models import *
from django.forms import formset_factory
from django_select2.forms import ModelSelect2Mixin

class AddProduct(forms.ModelForm):
    class Meta:
        model = PlusProducts
        fields = [
            'title',
            'name',
            'price_f1',
            #'ammount',
            'discount',
            'product_description',
            'profile_image',
            'document',
            'type',
        ]
        widgets = {
            #'document': ImageThumbnailFileInput,
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Заголовок'}),
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Назва'}),
            'price_f1': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Ціна'}),
            #'ammount': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': '0'}),
            'discount': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Знижка'}),
            #'profile_image': forms.FileField(attrs={'class': 'form-control'}),
            'product_description': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Опис товару'}),
            'type': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Тип'}),
        }

# class ReviewForm(forms.ModelForm):
#     class Meta:
#         model = Response
#         fields = [
#             'title',
#             'description',
#         ]
#
#         widgets = {
#             'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Заголовок'}),
#             'description': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Опис'}),
#         }

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = [
            'ammount'
        ]