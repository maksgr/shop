"""UniversitySiteLNU URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from catalog import views

urlpatterns = [
    path('add/', views.add_products, name='add_products'),
    path('catalog/', views.database_products_all, name='catalog'),
    path('tovar/<int:record_id>/', views.tovar, name='tovar'),
    path('card/', views.card, name='card'),
    path('ratings/', include('star_ratings.urls', namespace='ratings')),
    path('order/', views.order, name='order'),
    path('response/', views.responseProduct, name='responseProduct'),
    path('edit/<int:record_id>/', views.editModel, name='editModel'),
    path('delete/<int:record_id>/', views.deleteModel, name='deleteModel')
    #path('del/<int:record_id>/', views.deleteProducts, name='deleteProducts')
    # path('productElectronics/', views.productElectronics, name='productElectronics'),
    # path('productRealestate/', views.productRealestate, name='productRealestate'),
    # path('productWork/', views.productWork, name='productWork'),
    # path('productsOther/', views.productsOther, name='productsOther'),
    # path('productsClothing/', views.productsClothing, name='productsClothing'),
    # path('productTransport/', views.productTransport, name='productTransport'),
]
