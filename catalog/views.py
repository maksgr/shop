from django.shortcuts import render, redirect
from catalog.models import *
from catalog.forms import *
from django.http import JsonResponse
from django.db.models import Q


def add_products(request):
    if request.method == "POST":
        add_product = AddProduct(request.POST, request.FILES)
        if add_product.is_valid():
            add_product.save()
            return render(request, 'products.html')
    else:
        add_product = AddProduct

    context = {
        'add_product': add_product
    }

    return render(request, 'add_priduct.html', context)

def searh(request):
    search_query = request.GET.get('search', '')
    if search_query:
        posts = PlusProducts.objects.filter(title__icontains=search_query)
        print(posts)
    else:
        posts = PlusProducts.objects.all()

    return render(request, 'products.html', {'posts': posts})

def database_products_all(request):
    search_query = request.GET.get('search', '')
    if search_query:
        products = PlusProducts.objects.filter(title__icontains=search_query)
    elif request.GET.get('category'):
        category = request.GET.get('category')
        products = PlusProducts.objects.filter(type=category, is_active=True)
    else:
        products = PlusProducts.objects.filter(is_active=True)

    context = {
        'products': products,
    }

    return render(request, 'products.html', context)

def productsOther(request):
    products_other = PlusProducts.objects.filter(type='other')

    context = {
        'products_other': products_other,
    }
    return render(request, 'products.html', context)

def tovar(request, record_id):
    products = PlusProducts.objects.get(id=record_id, is_active=True)
    res = ResponseProduct.objects.filter(fk_product_id=record_id, is_active=True)

    sesion_key = request.session.session_key
    if not sesion_key:
        request.session.cycle_key()

    context = {
        'products': products,
        'res': res
    }

    return render(request, 'tovar.html', context)

def card(request):
    return render(request, 'card.html')

def order(request):
    return_dict = dict()
    sesion_key = request.session.session_key
    print(request.POST)
    data = request.POST
    product_id = data.get("product_id")
    ammount = data.get("number")
    is_delete = data.get("is_delete")
    r = data.get("deleteRes")

    if is_delete == 'true':
        prodOrder = Order.objects.get(id=r) # іd створення замовдення
        prodOrder.delete()
    else:
        new_product, creater = Order.objects.get_or_create(session_key=sesion_key, name_product_id=product_id, defaults={'ammount': ammount})
        if not creater:
            new_product.ammount += int(ammount)
            new_product.save(force_update=True)

    product_in_basket = Order.objects.filter(session_key=sesion_key, is_active=True)
    product_total_nmb = product_in_basket.count()
    return_dict['product_total_nmb'] = product_total_nmb

    return_dict['productionspr'] = list()

    for item in product_in_basket:
        product_dict = dict()
        product_dict['id'] = item.id
        product_dict['name_product'] = item.name_product.name
        product_dict['price'] =item.price
        product_dict['ammount'] = item.ammount
        return_dict['productionspr'].append(product_dict)

    return JsonResponse(return_dict)

def responseProduct(request):
    return_res = dict()
    data = request.POST
    product_id = data.get("product_id")
    text = data.get("t")
    is_delete = data.get("is_delete")
    resp_id = data.get("resp")

    if is_delete == 'true':
        delResp = ResponseProduct.objects.get(id=resp_id)
        delResp.delete()
    else:
        respon = ResponseProduct.objects.create(fk_product_id=product_id, description=text)
        respon.save()

    product_in_response = ResponseProduct.objects.filter(is_active=True)
    product_total_response = product_in_response.count()
    return_res['product_total_response'] = product_total_response

    # return_res['product_resp_qwer'] = list()
    #
    # for prod in product_in_response:
    #     product_res = dict()
    #     product_res['id'] = prod.id
    #     product_res['name_product'] = prod.fk_product.name
    #     product_res['text_res'] = prod.description
    #     product_res['product_resp_qwer'].append(return_res)

    #return render(request, 'tovar.html', {'product_in_response': product_in_response})
    #return
    return JsonResponse(return_res)

def editModel(request, record_id):
    my_record = PlusProducts.objects.get(id=record_id, is_active=True)
    if request.method == "POST":
        form = AddProduct(request.POST, instance=my_record)
        if form.is_valid():
            form.save()
    else:
        form = AddProduct(instance=my_record)

    context = {
        'my_record': my_record,
        'form': form
    }

    return render(request, 'edit.html', context)

def deleteModel(request, record_id):
    delProd = PlusProducts.objects.get(id=record_id, is_active=True)
    delProd.delete()

    return redirect('/catalog/catalog')
