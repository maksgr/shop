import os
from django_mysql.models import EnumField
from django.db import models
from star_ratings.models import AbstractBaseRating
from django.db.models.signals import post_save

from shop.settings import AUTH_USER_MODEL

def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)

class PlusProducts(models.Model):
    title = models.CharField(db_index=True, verbose_name="Заголовок", max_length=70 ,blank=False)
    name = models.CharField(db_index=True, verbose_name="Назва продукту", max_length=50 ,blank=False)
    price_f1 = models.FloatField(verbose_name='Вартість товару', blank=False)
    # price = models.PositiveIntegerField(verbose_name='Ціна товару', default=0)
    #ammount = models.PositiveIntegerField(verbose_name='Кількість товару', default=0)
    discount = models.PositiveIntegerField(verbose_name='Знижка', blank=False)
    product_description = models.CharField(db_index=True, verbose_name="Опис продукту", max_length=500, blank=False)
    profile_image = models.ImageField(blank=True, null=True, verbose_name="Завантажити зображення")
    document = models.ImageField(blank=True, null=True, verbose_name="Завантажити зображення")
    is_active = models.BooleanField(default=True)
    type = EnumField(verbose_name='Тип товару', choices=[
        ('work', 'Робота'),
        ('realestate', 'Нерухомість'),
        ('transport', 'Транспорт'),
        ('electronics', 'Електроніка'),
        ('clothing', 'Одяг'),
        ('other', 'Інше'),
    ])
    created_at = models.DateTimeField(verbose_name="Дата та час створення", auto_now_add=True)
    #timestamp = models.DateTimeField(verbose_name="Дата та час створення", auto_now_add=True)
    created_by = models.ForeignKey(AUTH_USER_MODEL, null=False, blank=False, default=1, on_delete=models.CASCADE)

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.title)

    class Meta:
        db_table = 'cat_plus_products'
        verbose_name = 'Прихід продукту'
        verbose_name_plural = 'Прихід продуктів'

class Order(models.Model):
    name_product = models.ForeignKey(PlusProducts, null=False, blank=False, default=None, on_delete=models.CASCADE)
    ammount = models.PositiveIntegerField(verbose_name='Кількість товару', default=0)
    price = models.FloatField(verbose_name='Вартість товару', default=0)
    session_key = models.CharField(max_length=128, null=False, blank=False, default=0)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(verbose_name="Дата та час замовлення", auto_now_add=True)
    created_by = models.ForeignKey(AUTH_USER_MODEL, null=False, blank=False, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return '%s (%s)' % (self.name_product, self.ammount)

    class Meta:
        db_table = 'cat_order'
        verbose_name = 'Замовлення товару'
        verbose_name_plural = 'Змовлення товару'

    def save(self, *args, **kwargs):
        price_per_iten = self.name_product.price_f1
        self.price_per_iten = price_per_iten
        self.total_price = int(self.ammount) * int(price_per_iten)
        self.price = self.total_price

        super(Order, self).save()

class MinusProducts(models.Model):
    title = models.CharField(db_index=True, verbose_name="Заголовок", max_length=70, blank=False)
    name = models.CharField(db_index=True, verbose_name="Назва продукту", max_length=50, blank=False)
    price_f1 = models.FloatField(verbose_name='Вартість товару', blank=False)
    # price = models.PositiveIntegerField(verbose_name='Ціна товару', default=0)
    #ammount = models.PositiveIntegerField(verbose_name='Кількість товару', default=0)
    discount = models.PositiveIntegerField(verbose_name='Знижка', default=0)
    product_description = models.CharField(db_index=True, verbose_name="Опис продукту", max_length=500, blank=False)
    type = EnumField(verbose_name='Тип товару', choices=[
        ('work', 'робота'),
        ('realestate', 'Нерухомість'),
        ('transport', 'Транспорт'),
        ('electronics', 'Електроніка'),
        ('clothing', 'Одяг'),
        ('other', 'Інше'),
    ])
    #session = models.ForeignKey('system.OwnerView', verbose_name='Owner View', null=True, on_delete=models.CASCADE)
    reason = models.CharField(max_length=500, verbose_name='Причина списання', null=True, blank=False)
    created_at = models.DateTimeField(verbose_name="Дата та час створення", auto_now_add=True)
    created_by = models.ForeignKey(AUTH_USER_MODEL, null=False, blank=False, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return '%s (%s)' % (self.title, self.name)

    class Meta:
        db_table = 'cat_minus_products'
        verbose_name = 'Списаний продукт'
        verbose_name_plural = 'Списані продукти'



class Money(models.Model):
    cash = models.FloatField(verbose_name='Готівка', default=0)
    credit = models.FloatField(verbose_name='Кредитка', default=0)
    owner = models.ForeignKey(AUTH_USER_MODEL, null=False, blank=False, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return self.owner.__str__()

    class Meta:
        db_table = 'cat_money'
        verbose_name = 'Гроші'
        verbose_name_plural = 'Гроші'

class ResponseProduct(models.Model):
    description = models.TextField(verbose_name='Опис', null=True, blank=False)
    fk_product = models.ForeignKey(PlusProducts, null=False, blank=False, default=None, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(verbose_name="Дата та час створення", auto_now_add=True)
    owner = models.ForeignKey(AUTH_USER_MODEL, null=False, blank=False, default=1, on_delete=models.CASCADE)

    def __str__(self):
        return self.description

    class Meta:
        db_table = 'cat_response_product'
        verbose_name = 'Відгук про товар'
        verbose_name_plural = 'Відгук про товар'


# class ProductInBasket(models.Model):
#     session_key = models.CharField(max_length=128)
#     order = models.ForeignKey(Order, null=False, blank=False, on_delete=models.CASCADE)
#     products = models.ForeignKey(PlusProducts, null=False, blank=False, on_delete=models.CASCADE)
#     nmb = models.IntegerField(default=1)
#     is_active = models.BooleanField(default=True)
#     price_per_item = models.FloatField(default=0)
#     total_prices = models.FloatField(default=0)
#     created_at = models.DateTimeField(verbose_name="Дата та час створення", auto_now_add=True)
#     created_by = models.ForeignKey(AUTH_USER_MODEL, null=False, blank=False, default=1, on_delete=models.CASCADE)
#
#     def __str__(self):
#         return self.products.name
#
#     class Meta:
#         db_table = 'cat_prod_on_basket'
#         verbose_name = 'Продукти у кошику'
#         verbose_name_plural = 'Продукти у кошику'

#
# class MyRating(AbstractBaseRating):
#    foo = models.TextField()
#
#    def __str__(self):
#        return self.foo